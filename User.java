package harshaa;

//import java.util.Scanner;

/**
 * Implementing a class to display the users details
 * 
 * @author harsha1
 */
//Marriage Status
enum MaritalStatus {
	SINGLE, MARRIED, DIVORCED, WIDOWED, COMPLICATED
}

//Gender
enum Gender {
	MALE, FEMALE
}

public class User {
	private static final String DIVORCED = null;
	private static final String maritalStatus = null;
	private static final String MARRIRED = null;
	private static final String SINGLE = null;
	private static final String WIDOWED = null;
	private static final String COMPLICATED = null;
	public static User currUser;
	public static boolean listOfMatches;
	int age;
	String name;
	String proffesion;
	String primaryHobbie;
	String hobbies[];
	String location;
	int weight;
	int height;
	int salary;
	String favplace;
	String favfood;
	String favplaces[];

	MaritalStatus maritalStatus1;
	Gender gender;
	String foodList[];
	String foodpreference;
	String Degree;
	Qualification qualification[];

	/**
	 * Created a constructor with arguments and calling all the instance variables
	 * 
	 * @param name
	 * @param age
	 * @param profession
	 * @param primaryHobbie
	 * @param hobbies
	 * @param location
	 */
	public User(String name, int age, String profession, String primaryHobbie, String[] hobbies, String location,
			int Weight, int Height, int salary, String favplace, String favfood, String[] favplaces,
			MaritalStatus maritalStatus1, Gender gender, String[] foodList, String foodpreference, String Degree,Qualification[] qualification) {
		this.name = name;
		this.age = age;
		this.proffesion = profession;
		this.primaryHobbie = primaryHobbie;
		this.hobbies = hobbies;
		this.location = location;
		this.weight = Weight;
		this.height = Height;
		this.salary = salary;
		this.favplace = favplace;
		this.favfood = favfood;
		this.favplaces = favplaces;
		this.maritalStatus1 = maritalStatus1;
		this.gender = gender;
		this.foodList = foodList;
		this.foodpreference = foodpreference;
		this.Degree = Degree;
		this.qualification = qualification;

		// this.maritalStatus1=maritalStatus1;
		// this.gender=gender;

		System.out.println(this);
	}

	@Override
	public String toString() {
		String StringToReturn = "";
		StringToReturn += "Name                      :   " + this.name + "\n";
		StringToReturn += "Age                       :   " + this.age + "\n";
		StringToReturn += "Profession                :   " + this.proffesion + "\n";
		StringToReturn += "PrimaryHobbie             :   " + this.primaryHobbie + "\n";
		StringToReturn += "Location                  :   " + this.location + "\n";
		StringToReturn += "Weight                    :   " + this.weight + "\n";
		StringToReturn += "Height                    :   " + this.height + "cm" + "\n";
		StringToReturn += "salary                    :   " + this.salary + "\n";
		StringToReturn += "favplace                  :   " + this.favplace + "\n";
		StringToReturn += "favfood                   :   " + this.favfood + "\n";
		StringToReturn += "MaritalStatus             :   " + this.maritalStatus1 + "\n";
		StringToReturn += "Gender                    :   " + this.gender + "\n";
		StringToReturn += "foodpreference            :   " + this.foodpreference + "\n";
		StringToReturn += "degree                    :   " + this.Degree + "\n";
		//StringToReturn += "qualification             :   " + this.qualification + "\n";
		// StringToReturn += "favhobbie : " + this.favhobbie + "\n";
		// StringToReturn += "foodList : " + this.foodList + "\n";
		String hobbyString = "[";
		for (int i = 0; i < this.hobbies.length; i++) {
			hobbyString += "\"" + this.hobbies[i] + "\" ";
		}
		hobbyString += "]";
		StringToReturn += "Hobbies                   :   " + hobbyString + "\n";

		// Iterate Array of favPlaces
		String favplacesString = "[";
		for (int i = 0; i < this.favplaces.length; i++) {
			favplacesString += "\"" + this.favplaces[i] + "\" ";
		}
		favplacesString += "]";
		StringToReturn += "favplaces                 :   " + favplacesString + "\n";
		
        //Iterate Array of Qualification
		String qualificationString = "[";
		for (int i = 0; i < this.qualification.length; i++) {
			qualificationString += "\"" + this.qualification[i] + "\" ";
		}
		favplacesString += "]";
		StringToReturn += "                 :   " + qualificationString + "\n";
		
		// Iterate Array of foodList
		String foodListString = "[";
		for (int i = 0; i < this.foodList.length; i++) {
			foodListString += "\"" + this.foodList[i] + "\" ";
		}
		foodListString += "]";
		return StringToReturn += "FoodList                  :   " + foodListString + "\n";
		
	}

	public void marriageMethod() {
		System.out.println("Enter the name:");
		switch (maritalStatus) {
		case "DIVORCED":
			System.out.println("DIVORCED");
			break;
		case "SINGLE":
			System.out.println("SINGLE");
			break;
		case "MARRIRED":
			System.out.println("MARRIED");
			break;
		case "WIDOWED":
			System.out.println("WIDOWED");
			break;
		default:
			break;

		}
	}
}
