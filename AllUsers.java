package harshaa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Implementing all users in the user list
 * 
 * @author harsha1
 */
public class AllUsers {
	
	// This is array list to contain all users in the users list
	private ArrayList<User> userList = new ArrayList<User>();

	// Default constructor
	public AllUsers() {
		this.userList = new ArrayList<User>();
		
	}
	

	/**
	 * This Method is used to create a new blunder user and checking the validations
	 * for the variables
	 * 
	 * @param name
	 * @param age
	 * @param profession
	 * @param PrimaryHobbie
	 * @param hobbies
	 * @return
	 */

	public boolean createNewBlunderUser(String name, int age, String profession, String PrimaryHobbie, String[] hobbies,
			String location, int weight, int height, int salary, String favplace, String favfood, String[] favplaces,
			MaritalStatus maritalStatus1, Gender gender, String[] foodList, String foodpreference, String Degree,Qualification [] qualification) {

		User addNewBlunderUser = new User(name, age, profession, PrimaryHobbie, hobbies, location, weight, height,
				salary, favplace, favfood, favplaces, maritalStatus1, gender, foodList, foodpreference, Degree, qualification);
		this.userList.add(addNewBlunderUser);
		return true;
	}

	/**
	 * This Method is used to find whether the search name is present in the users
	 * list
	 * 
	 * @param username
	 * @return
	 */
	public boolean findMatchesByUserName(String username) {
		for (User currUser : userList) {
			if (currUser.name.contains(username)) {
				User founderuser = User.currUser;
				System.out.println("The user " + founderuser + " is present in the userslist");
			}
		}
		return false;
	}

	/**
	 * This method is used to find the users matched by searched location
	 * 
	 * @param area
	 * @return
	 */
	public ArrayList<User> findMatchByLocation(String area) {
		ArrayList<User> locationList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.location == area)

				locationList.add(curruser);
		}
		return locationList;
	}

	/**
	 * This method is used to find the users matched by searched name
	 * 
	 * @param name
	 * @return
	 */
	public ArrayList<User> findMatchByName(String searchname) {
		ArrayList<User> nameList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.name == searchname)
				nameList.add(curruser);
		}
		return nameList;
	}

	/**
	 * This method is used to find the users matched by searched profession
	 * 
	 * @param searchProfession
	 * @return
	 */
	public ArrayList<User> findMatchByProfession(String searchProfession) {
		ArrayList<User> professionList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.proffesion == searchProfession)
				professionList.add(curruser);
		}
		return professionList;
	}

	/**
	 * This method is used to find the users matched by searched Hobby
	 * 
	 * @param searchHobby
	 * @return
	 */
	public ArrayList<User> findMatchByPrimaryHobby(String searchHobby) {
		ArrayList<User> hobbyList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.primaryHobbie == searchHobby)
				hobbyList.add(curruser);
		}
		return hobbyList;

	}

	/**
	 * This helper method to find the users, if one of the search hobby[] is present
	 * in the users hobby[]
	 * 
	 * @param hobbies
	 * @return
	 */
	public ArrayList<User> findMatchByHobbiesOr(String hobbies[]) {
		ArrayList<User> hobbiesORList = new ArrayList<User>();
		for (int i = 0; i < userList.size(); i++) {
			User newuser = userList.get(i);
			for (int k = 0; k < newuser.hobbies.length; k++) {
				for (int j = 0; j < hobbies.length; j++) {
					if (newuser.hobbies[k] == hobbies[j]) {
						hobbiesORList.add(newuser);
					}

				}
			}
		}
		return hobbiesORList;
	}

	/**
	 * This method is used to find maxWeight of user
	 * 
	 * @param Maxweight
	 * @return
	 */
	public ArrayList<User> findMatchBymaxWeight(int Maxweight) {
		ArrayList<User> MaxweightList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.weight > Maxweight)
				MaxweightList.add(curruser);
		}
		return MaxweightList;

	}

	/**
	 * This Method is used to find minWeight of user
	 * 
	 * @param Minweight
	 * @return
	 */
	public ArrayList<User> findMatchByminWeight(int Minweight) {
		ArrayList<User> MinweightList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.weight < Minweight)
				MinweightList.add(curruser);
		}
		return MinweightList;

	}

	/**
	 * This Method is used to find MinimumHeight of user
	 * 
	 * @param MinHeight
	 * @return
	 */
	public ArrayList<User> findMatchByminHeight(int MinHeight) {
		ArrayList<User> MinHeightList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.weight < MinHeight)
				MinHeightList.add(curruser);
		}
		return MinHeightList;

	}

	/**
	 * This method is used to find Maximum Height of user
	 * 
	 * @param MaxHeight
	 * @return
	 */
	public ArrayList<User> findMatchBymaxHeight(int MaxHeight) {
		ArrayList<User> MaxHeightList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.weight > MaxHeight)
				MaxHeightList.add(curruser);
		}
		return MaxHeightList;
	}

	/**
	 * This Method is used to find the Users Age.
	 * 
	 * @param age
	 * @return
	 */
	public ArrayList<User> findMatchByage(int age) {
		ArrayList<User> ageList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.age == age) {
				ageList.add(curruser);

			}

		}
		return ageList;

	}

	/**
	 * This Method is used to check the users in between the age limit.
	 * 
	 * @param maxAge
	 * @param minAge
	 * @return
	 */
	public ArrayList<User> findMatchBybetweenage(int maxAge, int minAge) {
		ArrayList<User> ageList = new ArrayList<User>();
		for (User curruser : userList) {
			if ((curruser.age) > maxAge && (curruser.age < minAge)) {
				ageList.add(curruser);
			}
		}
		return ageList;

	}

	/**
	 * This method is used to find the all users maxAge.
	 * 
	 * @param Maxage
	 * @return
	 */
	public ArrayList<User> findmatchBymaxage(int Maxage) {
		ArrayList<User> MaxageList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.age > Maxage)
				MaxageList.add(curruser);
		}
		return MaxageList;
	}

	/**
	 * This method is used to find all the Users minAge
	 * 
	 * @param Minage
	 * @return
	 */
	public ArrayList<User> findmatchByminage(int Minage) {
		ArrayList<User> MinageList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.age > Minage)
				MinageList.add(curruser);
		}
		return MinageList;
	}

	/**
	 * This method is used to find all the users salary
	 * 
	 * @param salary
	 * @return
	 */
	public ArrayList<User> findmatchBysalary(int salary) {
		ArrayList<User> salaryList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.salary > salary)
				salaryList.add(curruser);
		}
		return salaryList;
	}

	/**
	 * This method is used to find all the users minimum salary
	 * 
	 * @param minsalary
	 * @return
	 */
	public ArrayList<User> findmatchByminsalary(int minsalary) {
		ArrayList<User> minsalaryList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.salary > minsalary)
				minsalaryList.add(curruser);
		}
		return minsalaryList;
	}

	/**
	 * This method is used to find all the users maximum salary
	 * 
	 * @param maxsalary
	 * @return
	 */
	public ArrayList<User> findmatchBymaxsalary(int maxsalary) {
		ArrayList<User> maxsalaryList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.salary > maxsalary)
				maxsalaryList.add(curruser);
		}
		return maxsalaryList;
	}

	/**
	 * This method is used to check all the users between salary
	 * 
	 * @param minsalary
	 * @param maxsalary
	 * @return
	 */
	public ArrayList<User> findMatchBybetweensalary(int minsalary, int maxsalary) {
		ArrayList<User> btwsalaryList = new ArrayList<User>();
		for (User curruser : userList) {
			if ((curruser.salary > minsalary) && (curruser.salary < maxsalary)) {
				btwsalaryList.add(curruser);
			}
		}
		return btwsalaryList;
	}

	/**
	 * This method is used to check searchFavplace of all users
	 * 
	 * @param searchfavplace
	 * @return
	 */
	public ArrayList<User> findMatchByfavplace(String searchfavplace) {
		ArrayList<User> favplaceList = new ArrayList<User>();
		for (User curruser : userList) {

			if (curruser.favplace == searchfavplace)
				favplaceList.add(curruser);
		}
		return favplaceList;

	}

	/**
	 * This method is used to check both the searched hobbie[] with all the users
	 * 
	 * @param hobbies
	 * @return
	 */
	public ArrayList<User> getHobbyAndList(String hobbies[]) {
		ArrayList<User> getHobbyAndlist = new ArrayList<User>();
		User newuser = null;
		String hobby1 = hobbies[0];
		String hobby2 = hobbies[1];
		for (User currentuser : this.userList) {
			if ((Arrays.asList(currentuser.hobbies).contains(hobby1)
					&& (Arrays.asList(currentuser.hobbies).contains(hobby2)))) {
				newuser = currentuser;
				getHobbyAndlist.add(newuser);
			}
		}
		return getHobbyAndlist;
	}

	/**
	 * This method is used to check both the searched favFood[] with all the users
	 * 
	 * @param favfood
	 * @return
	 */
	public ArrayList<User> getfavFood(String favfood) {
		ArrayList<User> favfoodList = new ArrayList<User>();
		for (User curruser : userList) {

			if (curruser.favfood == favfood)
				favfoodList.add(curruser);
		}
		return favfoodList;

	}

	/**
	 * This method is used to check both the favPlacesand in the users
	 * 
	 * @param favplaces
	 * @return
	 */
	public ArrayList<User> getfavplacesAndList(String favplaces[]) {
		ArrayList<User> getfavplacesAndlist = new ArrayList<User>();
		User newuser = null;
		String favplaces1 = favplaces[0];
		String favplaces2 = favplaces[1];
		for (User currentuser : this.userList) {
			if ((Arrays.asList(currentuser.favplaces).contains(favplaces1)
					&& (Arrays.asList(currentuser.favplaces).contains(favplaces2)))) {
				newuser = currentuser;
				getfavplacesAndlist.add(newuser);
			}
		}
		return getfavplacesAndlist;

	}

	/**
	 * This method is used to check the both foodList in the user.
	 * 
	 * @param foodList
	 * @return
	 */
	public ArrayList<User> getfoodAndList(String foodList[]) {
		ArrayList<User> getfoodAndlist = new ArrayList<User>();
		User newuser = null;
		String foods1 = foodList[0];
		String foods2 = foodList[1];
		for (User currentuser : this.userList) {
			if ((Arrays.asList(currentuser.foodList).contains(foods1)
					&& (Arrays.asList(currentuser.foodList).contains(foods2)))) {
				newuser = currentuser;
				getfoodAndlist.add(newuser);
			}
		}
		return getfoodAndlist;

	}

	/**
	 * This method is used to check one of the favPlacesOr in the users
	 * 
	 * @param favplaces
	 * @return
	 */
	public ArrayList<User> findMatchByFavPlacesOr(String favplaces[]) {
		HashSet<User> favplacesORList = new HashSet<User>();
		for (int i = 0; i < userList.size(); i++) {
			User newuser = userList.get(i);
			for (int k = 0; k < newuser.favplaces.length; k++) {
				for (int j = 0; j < favplaces.length; j++) {
					if (newuser.favplaces[k] == favplaces[j]) {
						favplacesORList.add(newuser);
					}
				}
			}
		}
		return new ArrayList<User>(favplacesORList);
	}

	/**
	 * This method is used to check one of the favFoodsOr in the users
	 * 
	 * @param favfood
	 * @return
	 */
	public ArrayList<User> findMatchByFavfoodOr(String favfood[]) {
		HashSet<User> favfoodORList = new HashSet<User>();
		for (int i = 0; i < userList.size(); i++) {
			User newuser = userList.get(i);
			for (int k = 0; k < newuser.favfood.length(); k++) {
				for (int j = 0; j < favfood.length; j++) {
					if (newuser.favfood == favfood[j]) {
						favfoodORList.add(newuser);
					}
				}
			}
		}
		return new ArrayList<User>(favfoodORList);
	}

	/**
	 * This method is used to check one of the favhobbieOr in the users
	 * 
	 * @param favhobbie
	 * @return
	 */
	public ArrayList<User> findMatchByFavhobbieOr(String favhobbie[]) {
		HashSet<User> favhobbieORList = new HashSet<User>();
		for (int i = 0; i < userList.size(); i++) {
			User newuser = userList.get(i);
			for (int k = 0; k < newuser.hobbies.length; k++) {
				for (int j = 0; j < favhobbie.length; j++) {
					if (newuser.hobbies[k] == favhobbie[j]) {
						favhobbieORList.add(newuser);
					}
				}
			}
		}
		return new ArrayList<User>(favhobbieORList);
	}

	/**
	 * This method is used to check all the users foodPreference.
	 * 
	 * @param foodpreference
	 * @return
	 */
	public ArrayList<User> findmatchByfoodpreference(String foodpreference) {
		ArrayList<User> foodpreferenceList = new ArrayList<User>();
		for (User curruser : userList) {
			if (curruser.foodpreference == foodpreference) {
				foodpreferenceList.add(curruser);
			}
		}
		return foodpreferenceList;
	}

	/**
	 * This method is used to check users department
	 * 
	 * @param Degree
	 * @return
	 */
	public ArrayList<User> findmathByDegree(String Degree) {
		ArrayList<User> DegreeList = new ArrayList<User>();
		for (User curruser : userList) {

			if (curruser.Degree == Degree)
				DegreeList.add(curruser);
		}
		return DegreeList;

	}
}	

