package harshaa;

import java.lang.String;
import java.util.ArrayList;

/**
 * This Main class to print all the users information
 * 
 * @author Harsha1
 */
public class Main {

	public static void main(String[] args) {
		// Creating an object for AllUsers class
		AllUsers Users = new AllUsers();
		Qualification q=new Qualification("B.tech", "Cse", "2022", "Vaagdevi College");
		Qualification q1=new Qualification("B.tech", "ECE", "2022", "Kits College");
		Qualification q2=new Qualification("Degree", "B.Com", "2022", "Vaagdevi College");
//		String harsha = "SINGLE";
//		User u = new User(MaritalStatus.valueOf(harsha));
//	u.marriageMethod();
		System.out.println("--------------Users Information-------------");
		System.out.println("=============================================");
		System.out.println("Frist User :");
		System.out.println("------------");
		// By using User class object, printing blunder users
		Users.createNewBlunderUser("harsha", 25, "Software Trainee", "Playing",
				new String[] { "Sports", "Swimming", "Playing" }, "Hyderabad", 60, 145, 600000, "Ooty", "Mutton",
				new String[] { "Ooty", "Warangal" }, MaritalStatus.SINGLE, Gender.FEMALE,
				new String[] { "Mutton", "Egg" }, "non-veg", "B.Tech",new Qualification [] {q1,q2});
		System.out.println("Second User :");
		System.out.println("------------");
		Users.createNewBlunderUser("Swapna", 21, "C", "Dancing", new String[] { "Dancing", "Singing", "Carroms" },
				"Karimnagar", 89, 152, 300000, "kamareddy", "Chicken", new String[] { "kamareddy", "warangal" },
				MaritalStatus.SINGLE, Gender.FEMALE, new String[] { "Biriyani", "Egg" }, "non-veg", "Degree",new Qualification [] {q1});
		System.out.println("Third User :");
		System.out.println("------------");
		Users.createNewBlunderUser("Yashu", 10, "Software Trainee", "Cooking",
				new String[] { "Cooking", "Dancing", "Playing" }, "Warangal", 79, 134, 600000, "Paris",
				"BabycornManchuria", new String[] { "Paris", "Warangal" }, MaritalStatus.SINGLE, Gender.MALE,
				new String[] { "Mutton", "Egg" }, "veg", "B.Tech",new Qualification [] {q2});
		System.out.println("Fourth User :");
		System.out.println("------------");
		Users.createNewBlunderUser("Sravani", 20, "Java Developer", "Dancing", new String[] { "Dancing", "Talking" },
				"Warangal", 60, 165, 500000, "Kamareddy", "Mutton", new String[] { "kamareddy", "warangal" },
				MaritalStatus.SINGLE, Gender.FEMALE, new String[] { "Fish", "Mutton" }, "non-veg", "B.Tech",new Qualification [] {q1});
		System.out.println("Fifth User :");
		System.out.println("------------");
		Users.createNewBlunderUser("Malli", 12, "Java Developer", "Movies",
				new String[] { "Eating", "Movies", "Dancing" }, "Warangal", 50, 162, 650000, "Paris", "Chicken",
				new String[] { "Paris", "warangal" }, MaritalStatus.MARRIED, Gender.MALE,
				new String[] { "Egg", "Chicken" }, "non-veg", "Degree",new Qualification [] {q2});
		System.out.println("Sixth User :");
		System.out.println("-------------");
		Users.createNewBlunderUser("Surya", 17, "Web Development", "Reading",
				new String[] { "Tv", "Reading", "Sports" }, "Warangal", 46, 156, 900000, "Ooty", "Mutton",
				new String[] { "Ooty", "Hyd" }, MaritalStatus.DIVORCED, Gender.MALE, new String[] { "Chicken", "Egg" },
				"veg", "B.Tech",new Qualification [] {q1});

		// Checking users with common location
		System.out.println("Searching location with all the users :");
		System.out.println("-----------------------------------");
		ArrayList<User> locationresults = Users.findMatchByLocation("Hyderabad");
		System.out.println(locationresults);

		// checking users with common name
		System.out.println("Searching  name with all the users :");
		System.out.println("-----------------------------------");
		ArrayList<User> nameresults = Users.findMatchByName("Yashu");
		System.out.println(nameresults);

		// checking users with searched profession
		System.out.println("Searching profession with all the users :");
		System.out.println("-----------------------------------");
		ArrayList<User> professionresults = Users.findMatchByProfession("Software Trainee");
		System.out.println(professionresults);

		// checking users with searched primaryHobby
		System.out.println("Searching the  primary hobby with all the users :");
		System.out.println("------------------------------------");
		ArrayList<User> primaryhobbyresults = Users.findMatchByPrimaryHobby("Carroms");
		System.out.println(primaryhobbyresults);

		// calling matchbyhobbiesoR method
		System.out.println("Searching  Array of Hobby with all the users :");
		System.out.println("-------------------------------------");
		ArrayList<User> hobbiesOR = Users.findMatchByHobbiesOr(new String[] { "Dancing", "Reading" });
		System.out.println(hobbiesOR);
		// Checking all the Users With maxiWeight
		System.out.println("info of users with the searched maximum weight is:");
		System.out.println("--------------------------------------");
		ArrayList<User> MaxweightList = Users.findMatchBymaxWeight(60);
		System.out.println(MaxweightList);
		// Checking all the Users with minWeight
		System.out.println("info of users with the searched minimum weight is:");
		System.out.println("--------------------------------------");
		ArrayList<User> MinweightList = Users.findMatchByminWeight(60);
		System.out.println(MinweightList);
		// Checking all the Users Height
		System.out.println("info of users with the searched  minimum height is:");
		System.out.println("--------------------------------------");
		ArrayList<User> MinHeightList = Users.findMatchByminHeight(6);
		System.out.println(MinHeightList);
		// Checking all the users with maxHeight
		System.out.println("info of the users  with the searched maximum height is:");
		System.out.println("--------------------------------------");
		ArrayList<User> MaxHeightList = Users.findMatchBymaxHeight(6);
		System.out.println(MaxHeightList);
		// Checking all the users with age
		System.out.println("info of the users with the similar age:");
		System.out.println("--------------------------------------");
		ArrayList<User> MaxageList = Users.findMatchByage(17);
		System.out.println(MaxageList);
		// Checking all the users between the age limit is 9 to 15.
		System.out.println("info of the users checking Between the age limit :");
		System.out.println("--------------------------------------");
		ArrayList<User> ageList = Users.findMatchBybetweenage(9, 15);
		System.out.println(ageList);
		// Checking all the users with Maximum age
		System.out.println("info of the users with the searched Maxmium age limit :");
		System.out.println("--------------------------------------");
		ArrayList<User> maxageList = Users.findmatchBymaxage(23);
		System.out.println(maxageList);
		// Checking all the users with Minimum age.
		System.out.println("info of the users with  the searched  Minimum age limit :");
		System.out.println("--------------------------------------");
		ArrayList<User> minageList = Users.findmatchByminage(23);
		System.out.println(minageList);
		// Checking all the users with salary
		System.out.println("info of the  users withthe searched  Manimum age limit :");
		System.out.println("--------------------------------------");
		ArrayList<User> salaryList = Users.findmatchBysalary(23);
		System.out.println(salaryList);
		// Checking all the users minimum salary
		System.out.println(" info of the users with searched Minimum salary :");
		System.out.println("--------------------------------------");
		ArrayList<User> minsalaryList = Users.findmatchByminsalary(400000);
		System.out.println(minsalaryList);
		// Checking all the users maximum salary
		System.out.println(" info of the users with searched maximum salary :");
		System.out.println("--------------------------------------");
		ArrayList<User> maxsalaryList = Users.findmatchBymaxsalary(800000);
		System.out.println(maxsalaryList);
		// Checking all the users between the salary limit is 9 to 15.
		System.out.println("info of the users checking Between the salary limit :");
		System.out.println("--------------------------------------");
		ArrayList<User> btwsalaryList = Users.findMatchBybetweensalary(300000, 900000);
		System.out.println(btwsalaryList);
		// Checking the users with common favPlace
		System.out.println("Here is the info of the users with common favplace");
		System.out.println("---------------------------------------------------");
		ArrayList<User> favplaceList = Users.findMatchByfavplace("Paris");
		System.out.println(favplaceList);
		// Checking both the searched Hobby[]
		System.out.println("Here is the info of the users with both the searched hobbies");
		System.out.println("---------------------------------------------------");
		ArrayList<User> getHobbyAndlist = Users.getHobbyAndList(new String[] { "Cooking", "Dancing" });
		System.out.println(getHobbyAndlist);
		// Checking the info of the users with same favFoods
		System.out.println("Here is the info of the users with same favfood");
		System.out.println("---------------------------------------------------");
		ArrayList<User> favfoodList = Users.getfavFood("Mutton");
		System.out.println(favfoodList);
		// Checking the info of the users with favPlacesOr
		System.out.println("Here is the info of the users with favplacesOr");
		System.out.println("---------------------------------------------------");
		ArrayList<User> favplacesORList = Users.findMatchByFavPlacesOr(new String[] { "Warangal", "hyd" });
		System.out.println(favplacesORList);
		// Checking the info of the users with favfoodsOr
		System.out.println("Here is the info of the users with favfoodOr");
		System.out.println("---------------------------------------------------");
		ArrayList<User> favfoodORList = Users.findMatchByFavfoodOr(new String[] { "Egg", "Chicken" });
		System.out.println(favfoodORList);
		// Checking the info of the users with favhobbieOr
		System.out.println("Here is the info of the users with favhobbieOr");
		System.out.println("---------------------------------------------------");
		ArrayList<User> favhobbieORList = Users.findMatchByFavhobbieOr(new String[] { "Tv", "Dancing" });
		System.out.println(favhobbieORList);
		// Checking the info of the users department
		System.out.println("Here is the info of the users Department");
		System.out.println("---------------------------------------------------");
		ArrayList<User> degreeList = Users.findmathByDegree("B.Tech");
		System.out.println(degreeList);

	}
}